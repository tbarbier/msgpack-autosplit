
#ifndef __OPTIONS_H__
#define __OPTIONS_H__ 1

#include "msgpack-autosplit.h"

int options_parse(AppContext * const context, int argc, char *argv[]);

#endif
